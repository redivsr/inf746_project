#include <Servo.h>
#include <TH02_dev.h>
#include "rgb_lcd.h"
#include <math.h>
#include <limits.h>

#include "jsmn.h"
#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"

#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 500
#define DEBOUNCE_TIME  500

/* Pin definition */
#define MOISTURE_PIN  A1
#define BUTTON_PIN    0
#define RELAY_PIN     5
#define SERVO_PIN     6
/* Servo angles */
#define SERVO_GAS_ANGLE         45
#define SERVO_ELECTRICITY_ANGLE  135

/* heater params */
float gasCost = -1;
float electricityCost = -1;
float currentTemperature = -1;
float targetTemperature = -1;
bool localButton = 0;
bool remoteButton = 0;
float waterTemperature = -1;
float moistureLevel = -1;
/* used for debouncing the button */
long last_pressed = 0;
/* Indicates whenever the cost or humidity changes */
bool paramsChanged = false;

Servo heaterSwitch;
rgb_lcd lcd;

enum heater_state {
    ON,
    OFF,
    GAS,
    ELETRICITY
};

heater_state current_state;
char str_heaterStatus = 'O';

/* JSON variables*/
size_t sizeOfJsonDocumentBuffer;
char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
char *pJsonStringToUpdate;
/* IoT objects handlers */
jsonStruct_t remoteButtonHandler;
jsonStruct_t waterTemperatureHandler;
jsonStruct_t targetTemperatureHandler;
jsonStruct_t heaterStatusHandler;
jsonStruct_t gasCostHandler;
jsonStruct_t electricityCostHandler;

/* Aws IoT variables*/
IoT_Error_t rc;
AWS_IoT_Client mqttClient;
ShadowConnectParameters_t scp;
ShadowInitParameters_t sp;
char certDirectory[] = "certs";
char HostAddress[255] = AWS_IOT_MQTT_HOST;
uint32_t port = AWS_IOT_MQTT_PORT;

/* Functions headers */
void applyCommand(char action, float value);
void readSerialCommand ();
void refreshDisplay();
void readSensors();
void turnEletricityHeaterOn();
void turnGasHeaterOn();
void turnHeaterOff();
void turnHeater(heater_state new_state);
void initializeJsonHandlers();
void prepareAwsShadowParams();
void shadow_update();

/* Aws shadow callbacks */
void gasCostCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void electricityCostCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void targetTemperatureCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void remoteButtonCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext);
void ShadowUpdateStatusCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
								const char *pReceivedJsonDocument, void *pContextData);

void setup() {
    Serial.begin(9600);
    while (!Serial);

    lcd.begin(16, 2);

    heaterSwitch.attach(SERVO_PIN);
    heaterSwitch.write(0);
    pinMode(RELAY_PIN, OUTPUT);
    pinMode(BUTTON_PIN, INPUT);

    prepareAwsShadowParams();

    rc = aws_iot_shadow_connect(&mqttClient, &scp);
    if(SUCCESS != rc) {
        Serial.print("Shadow Connection Error: ");
        Serial.println(rc);
        return ;
    }

    rc = aws_iot_shadow_set_autoreconnect_status(&mqttClient, true);
    if(SUCCESS != rc) {
        Serial.print("Unable to set Auto Reconnect to true: ");
        Serial.println(rc);
        return ;
    }

    initializeJsonHandlers();
}

void loop() {
    readSerialCommand();
    shadow_update();
    readSensors();
    if ((remoteButton || localButton) && currentTemperature < targetTemperature) {
        turnHeater(ON);
    } else {
        turnHeater(OFF);
    }
    refreshDisplay();
    delay(30);
}

/* Turn the heater off/on if there are changes in the state or costs */
void turnHeater(heater_state new_state) {
    if (current_state != new_state) {
        if (new_state == OFF) {
            turnHeaterOff();
            current_state = OFF;
            str_heaterStatus = 'O';
        } else if (current_state == OFF || paramsChanged) {
            paramsChanged = false;
            float gasRelativeCost = gasCost * fabs(1 - ((moistureLevel)/1000)*8);
            if (gasRelativeCost < electricityCost) {
                turnGasHeaterOn();
                current_state = GAS;
                str_heaterStatus = 'G';
            } else {
                turnEletricityHeaterOn();
                current_state = ELETRICITY;
                str_heaterStatus = 'E';
            }
        }
    }
}

/* Turn the heater Relay Off */
void turnHeaterOff() {
    digitalWrite(RELAY_PIN, LOW);
}

/* Turn gas heater on */
void turnGasHeaterOn() {
    digitalWrite(RELAY_PIN, HIGH);
    heaterSwitch.write(SERVO_GAS_ANGLE);
}

/* Turn eletricity heater on */
void turnEletricityHeaterOn() {
    digitalWrite(RELAY_PIN, HIGH);
    heaterSwitch.write(SERVO_ELECTRICITY_ANGLE);
}

/* Read all sensors: temperature, moisture and button */
void readSensors() {
    currentTemperature = TH02.ReadTemperature();

    /* button reading and debounce */
    int now = millis();
    if (last_pressed < now - DEBOUNCE_TIME && digitalRead(BUTTON_PIN)) {
        localButton = !localButton;
        last_pressed = now;
    }

    /* moisture sensor reading */
    int tmp = analogRead(A1);
    if (tmp != moistureLevel) {
        paramsChanged = true;
        moistureLevel = tmp;
    }
}

/* Refresh display data */
void refreshDisplay() {
    lcd.clear();
    lcd.setCursor(0, 0);
    switch(current_state) {
    case OFF:
        lcd.print('O');
        break;
    case GAS:
        lcd.print('G');
        break;
    case ELETRICITY:
        lcd.print('E');
        break;
    }
    lcd.setCursor(6, 0);
    char *buf = (char*) malloc (5 * sizeof(char));
    sprintf(buf, "%.2f C", currentTemperature);
    lcd.print(buf);
    lcd.setCursor(6, 1);
    sprintf(buf, "%.2f C", targetTemperature);
    lcd.print(buf);
    free(buf);
}

/* Read commands from USB serial */
void readSerialCommand () {
    if (Serial.available() > 0) {
        /* get incoming byte: */
        char action = Serial.read();
        float value =  Serial.parseFloat();

        applyCommand(action, value);
    }
}

/* Updates the system parameters */
void applyCommand(char action, float value) {
    switch (action) {
    case 'G':
        gasCost = value;
        paramsChanged = true;
        break;
    case 'E':
        electricityCost = value;
        paramsChanged = true;
        break;
    case 'T':
        targetTemperature = value;
        break;
    case 'R':
        remoteButton = (int) value;
        break;
    case 'L':
        localButton = (int) value;
        break;
    case 'W':
        waterTemperature = value;
        break;
    case 'M':
        moistureLevel = value;
        paramsChanged = true;
        break;
    }
}

/* Callback called to ack the update sent */
void ShadowUpdateStatusCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
								const char *pReceivedJsonDocument, void *pContextData) {
    IOT_UNUSED(pThingName);
    IOT_UNUSED(action);
    IOT_UNUSED(pReceivedJsonDocument);
    IOT_UNUSED(pContextData);

    if(SHADOW_ACK_TIMEOUT == status) {
        Serial.println("Update Timeout");
    } else if(SHADOW_ACK_REJECTED == status) {
        Serial.println("Update Rejected");
    } else if(SHADOW_ACK_ACCEPTED == status) {
        Serial.println("Update Accepted");
    }
}

/* Callback called when remoteButton is changed in cloud */
void remoteButtonCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        Serial.println("Remote button updated");
    }
}

/* Callback called when targetTemperature is changed in cloud */
void targetTemperatureCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        Serial.println("Target temperature updated");
    }
}

/* Callback called when electricityCost is changed in cloud */
void electricityCostCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        Serial.println("Electricity cost updated");
        paramsChanged = true;
    }
}

/* Callback called when gasCost is changed in cloud */
void gasCostCallback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        Serial.println("Gas cost updated");
        paramsChanged = true;
    }
}

/* Try to update shadow device */
void shadow_update() {
    if( NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || SUCCESS == rc) {
        rc = aws_iot_shadow_yield(&mqttClient, 200);
        if(NETWORK_ATTEMPTING_RECONNECT == rc) {
            return;
        }
        else {
            rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
            if(SUCCESS == rc) {
                rc = aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 6, &waterTemperatureHandler,
                                                &remoteButtonHandler, &heaterStatusHandler, &targetTemperatureHandler,
                                                &electricityCostHandler, &gasCostHandler);
                if(SUCCESS == rc) {
                    rc = aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
                    if(SUCCESS == rc) {
                        /* Sends delta to AWS */
                        rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                                                ShadowUpdateStatusCallback, NULL, 4, true);
                    }
                } else {
                    Serial.print("Failed to report: ");
                    Serial.println(rc);
                }
            } else {
                Serial.print("Failed to init report: ");
                Serial.println(rc);
            }
        }
    }
    sleep(1);
}

/* Initialize json handlers and registers its callbacks */
void initializeJsonHandlers() {
    remoteButtonHandler.cb = remoteButtonCallback;
    remoteButtonHandler.pData = &remoteButton;
    remoteButtonHandler.pKey = "remote_button";
    remoteButtonHandler.type = SHADOW_JSON_BOOL;

    waterTemperatureHandler.cb = NULL;
    waterTemperatureHandler.pKey = "current_temperature";
    waterTemperatureHandler.pData = &currentTemperature;
    waterTemperatureHandler.type = SHADOW_JSON_FLOAT;

    targetTemperatureHandler.cb = targetTemperatureCallback;
    targetTemperatureHandler.pKey = "target_temperature";
    targetTemperatureHandler.pData = &targetTemperature;
    targetTemperatureHandler.type = SHADOW_JSON_FLOAT;

    heaterStatusHandler.cb = NULL;
    heaterStatusHandler.pKey = "water_heater_status";
    heaterStatusHandler.pData = &str_heaterStatus;
    heaterStatusHandler.type = SHADOW_JSON_STRING;

    gasCostHandler.cb = gasCostCallback;
    gasCostHandler.pKey = "gas_cost";
    gasCostHandler.pData = &gasCost;
    gasCostHandler.type = SHADOW_JSON_FLOAT;

    electricityCostHandler.cb = electricityCostCallback;
    electricityCostHandler.pKey = "electric_cost";
    electricityCostHandler.pData = &electricityCost;
    electricityCostHandler.type = SHADOW_JSON_FLOAT;

    /* Registering Handlers */
    rc = aws_iot_shadow_register_delta(&mqttClient, &remoteButtonHandler);
    if(SUCCESS != rc) {
        Serial.println("Shadow Register Delta Error");
    }

    rc = aws_iot_shadow_register_delta(&mqttClient, &targetTemperatureHandler);
    if(SUCCESS != rc) {
        Serial.println("Shadow Register Delta Error");
    }

    rc = aws_iot_shadow_register_delta(&mqttClient, &gasCostHandler);
    if(SUCCESS != rc) {
        Serial.println("Shadow Register Delta Error");
    }

    rc = aws_iot_shadow_register_delta(&mqttClient, &electricityCostHandler);
    if(SUCCESS != rc) {
        Serial.println("Shadow Register Delta Error");
    }
    Serial.println("Handlers initialized");
}

/* Load certificates and prepares shadow parameters */
void prepareAwsShadowParams() {
    sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

    rc = FAILURE;

    /* Prepare certificate path */
    char rootCA[PATH_MAX + 1];
    char clientCRT[PATH_MAX + 1];
    char clientKey[PATH_MAX + 1];
    char CurrentWD[PATH_MAX + 1];

    strcpy(CurrentWD, "/sketch");
    snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_ROOT_CA_FILENAME);
    snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_CERTIFICATE_FILENAME);
    snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_PRIVATE_KEY_FILENAME);

    Serial.println(rootCA);
    Serial.println(clientCRT);
    Serial.println(clientKey);

    sp = ShadowInitParametersDefault;
    sp.pHost = AWS_IOT_MQTT_HOST;
    sp.port = AWS_IOT_MQTT_PORT;
    sp.pClientCRT = clientCRT;
    sp.pClientKey = clientKey;
    sp.pRootCA = rootCA;
    sp.enableAutoReconnect = false;
    sp.disconnectHandler = NULL;

    rc = aws_iot_shadow_init(&mqttClient, &sp);
    if(SUCCESS != rc) {
        Serial.print("Shadow Init Error: ");
        Serial.println(rc);
        return;
    }
    scp = ShadowConnectParametersDefault;
    scp.pMyThingName = AWS_IOT_MY_THING_NAME;
    scp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
    scp.mqttClientIdLen = (uint16_t) strlen(AWS_IOT_MQTT_CLIENT_ID);
    Serial.println("Certs loaded and shadow parameters set");
}