# This project is part of course INF-746 IoT and Cloud. #

### What is this repository for? ###

The project consists of an water heater system that
can be controlled locally or over AWS IoT shadow.

### How do I get set up? ###

To get it running follow the steps
* Copy the file aws_iot_config.h to your library/aws-iot-sdk/
* Copy the certs to /sketch/certs in your Intel Edison 
* Run the main program project.ino
* On AWS IoT use the MQTT Client terminal to communicate with Edison

### Basic commands ###

* Turn on/off the system using the local push button
* To change target_temperature, gas_cost, electric_cost, or 
	button state, you can either send the desired value via
	aws or you can set it via Serial Terminal
* Commands in the terminal	
	* G <float>  - sets the gas cost 
	* E <float>  - sets the electric cost 
	* T <float>  - sets the target temperature
	* R <bool>   - sets the remote button state
	* L <bool>   - sets the local button state
	* M <float>  - sets the moisture level (simulate)
	* W <float>  - sets the water temperature (simulate)